package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testInputFirstThrow() throws BowlingException{
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(firstThrow, frame.getFirstThrow());
	}
	
	@Test
	public void testInputSecondThrow() throws BowlingException{
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(secondThrow, frame.getSecondThrow());
	}
	
	@Test
	public void testFrameScoreThrow() throws BowlingException {
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow,secondThrow);
		assertEquals(firstThrow+secondThrow,frame.getScore());
	}
	
	@Test
	public void testInputBonus() throws BowlingException {
		int bonus = 3;
		Frame frame = new Frame(2,4);
		frame.setBonus(bonus);
		assertEquals(bonus, frame.getBonus());
	}
	
	@Test
	public void testFrameShouldBeSpare() throws BowlingException {
		Frame frame = new Frame(1,9);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testFrameScoreThrowWithBonusSpareBonus() throws BowlingException {
		Frame frame = new Frame(1,9);
		frame.setBonus(3);
		assertEquals(13, frame.getScore());
	}
	
	@Test(expected = BowlingException.class)
	public void frameShoulRaiseException() throws BowlingException{
		new Frame(10, 1);
	}
	
	@Test
	public void testFrameShouldBeStrike() throws BowlingException {
		Frame frame = new Frame(10,0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testFrameScoreThrowWithStrikeBonus() throws BowlingException {
		Frame frame = new Frame(10,0);
		frame.setBonus(9);
		assertEquals(19, frame.getScore());
	}
}
