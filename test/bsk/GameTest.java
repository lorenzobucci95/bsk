package bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void testAdditionFrame() throws BowlingException {
		ArrayList<Frame> frames = new ArrayList<>(10);
		Game game = new Game();
		
		Frame frame1 = new Frame(1,5);
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		
		frames.add(frame1);
		frames.add(frame2);
		frames.add(frame3);
		frames.add(frame4);
		frames.add(frame5);
		frames.add(frame6);
		frames.add(frame7);
		frames.add(frame8);
		frames.add(frame9);
		frames.add(frame10);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertTrue(frames.equals(game.getFrames()));
	}
	
	
	@Test
	public void testShouldBeReturnSpecificFrame() throws BowlingException {
		ArrayList<Frame> frames = new ArrayList<>(10);
		Game game = new Game();
		
		Frame frame1 = new Frame(1,5);
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		
		frames.add(frame1);
		frames.add(frame2);
		frames.add(frame3);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		
		assertEquals(frame2, game.getFrameAt(1));
	}
	
	@Test
	public void testCalculationScore() throws BowlingException {
		Game game = new Game();
		
		Frame frame1 = new Frame(1,5);
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(81,game.calculateScore());
	}
	
	@Test
	public void testCalculationScoreWithSpareBonus() throws BowlingException {
		Game game = new Game();
		
		Frame frame1 = new Frame(1,9);
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(88,game.calculateScore());
		
	}
	
	@Test
	public void testCalculationScoreWithSpareLastFrame() throws BowlingException {
		Game game = new Game();
		
		Frame frame1 = new Frame(2,6);
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(1,9);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(85,game.calculateScore());
	}
	
	@Test
	public void testCalculationScoreWithStrikeBonus() throws BowlingException {
		Game game = new Game();
		
		Frame frame1 = new Frame(10,0);
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(94,game.calculateScore());
	}
	
	@Test
	public void testCalculationScoreWithStrikeFollowedByFrame() throws BowlingException {
		Game game = new Game();
		
		Frame frame1 = new Frame(10,0);
		Frame frame2 = new Frame(4,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(103,game.calculateScore());
	}
	
	@Test
	public void testCalculationScoreWithMultipleStrikes() throws BowlingException {
		Game game = new Game();
		
		Frame frame1 = new Frame(10,0);
		Frame frame2 = new Frame(10,0);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(112,game.calculateScore());
	}
	
	@Test
	public void testCalculationScoreWithMultipleSpares() throws BowlingException {
		Game game = new Game();
		
		Frame frame1 = new Frame(8,2);
		Frame frame2 = new Frame(5,5);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,6);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(98,game.calculateScore());
	}
	
	@Test
	public void testInputFirstBonusThrow() throws BowlingException {
		Game game = new Game();
		game.setFirstBonusThrow(7);
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void testCalculationScoreWithSpareAsTheLastFrame() throws BowlingException {
		Game game = new Game();
		
		Frame frame1 = new Frame(1,5);
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(2,8);
		
		game.setFirstBonusThrow(7);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(90,game.calculateScore());
	}
	
	@Test
	public void testInputSecondBonusThrow() throws BowlingException {
		Game game = new Game();
		game.setSecondBonusThrow(2);
		assertEquals(2, game.getSecondBonusThrow());
	}
	
	@Test
	public void testCalculationScoreWithStrikeAsTheLastFrame() throws BowlingException {
		Game game = new Game();
		
		Frame frame1 = new Frame(1,5);
		Frame frame2 = new Frame(3,6);
		Frame frame3 = new Frame(7,2);
		Frame frame4 = new Frame(3,6);
		Frame frame5 = new Frame(4,4);
		Frame frame6 = new Frame(5,3);
		Frame frame7 = new Frame(3,3);
		Frame frame8 = new Frame(4,5);
		Frame frame9 = new Frame(8,1);
		Frame frame10 = new Frame(10,0);
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(92,game.calculateScore());
	}
	
	@Test
	public void testCalculationBestScore() throws BowlingException {
		Game game = new Game();
		
		Frame frame1 = new Frame(10,0);
		Frame frame2 = new Frame(10,0);
		Frame frame3 = new Frame(10,0);
		Frame frame4 = new Frame(10,0);
		Frame frame5 = new Frame(10,0);
		Frame frame6 = new Frame(10,0);
		Frame frame7 = new Frame(10,0);
		Frame frame8 = new Frame(10,0);
		Frame frame9 = new Frame(10,0);
		Frame frame10 = new Frame(10,0);
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		game.addFrame(frame1);
		game.addFrame(frame2);
		game.addFrame(frame3);
		game.addFrame(frame4);
		game.addFrame(frame5);
		game.addFrame(frame6);
		game.addFrame(frame7);
		game.addFrame(frame8);
		game.addFrame(frame9);
		game.addFrame(frame10);
		
		assertEquals(300,game.calculateScore());
	}
	
}
