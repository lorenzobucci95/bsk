package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {
	
	private ArrayList<Frame> frames;
	private int firstBonusThrow;
	private int secondBonusThrow;
	private static final int MAX_FRAMES = 10;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<>(MAX_FRAMES);
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) {
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frames.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() {
		int score = 0;
		for(int i=0;i<frames.size();i++) {
			if(frames.get(i).isSpare()) {
				if(i==MAX_FRAMES-1) {
					frames.get(i).setBonus(getFirstBonusThrow());
					score += frames.get(i).getScore();
				}else {
					frames.get(i).setBonus(frames.get(i+1).getFirstThrow());
					score += frames.get(i).getScore();
				}
			}else if(frames.get(i).isStrike()) {
				if(i==MAX_FRAMES-2) {
					frames.get(i).setBonus(frames.get(i+1).getFirstThrow()+getFirstBonusThrow());
					score += frames.get(i).getScore();
				}else if(i==MAX_FRAMES-1) {
					frames.get(i).setBonus(getFirstBonusThrow()+getSecondBonusThrow());
					score += frames.get(i).getScore();
				}else if(frames.get(i+1).isStrike()) {
					frames.get(i).setBonus(frames.get(i+1).getFirstThrow()+frames.get(i+2).getFirstThrow());
					score += frames.get(i).getScore();
				}else {
					frames.get(i).setBonus(frames.get(i+1).getFirstThrow()+frames.get(i+1).getSecondThrow());
					score += frames.get(i).getScore();
				}
			}else {
				score += frames.get(i).getScore();
			}
		}
		return score;
	}

	public List<Frame> getFrames() {
		return frames;
	}

}
